<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CabinetController extends Controller
{
    public function welcome()
    {
        return view('frontend.index');
    }

    public function about()
    {
        return view('frontend.about');
    }

    public function laravelDoc()
    {
        return redirect('https://laravel.com/docs/5.7');
    }

}
