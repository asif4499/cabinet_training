<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
</head>
<body>
    <form action="" method="post">
        <input type="email" placeholder="Enter Your Email"><br>
        <input type="password" placeholder="Enter Your Password"><br>
        <button type="button"><a href="{{ url('/admin') }}">Login</a></button>
    </form>
</body>
</html>
